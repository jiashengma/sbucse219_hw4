package jClassDesigner.gui;

import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jia Sheng Ma
 * @version 1.0
 */
public class AddInterfaceUI extends VBox{
     
    Label interfaceName_lb;
    TextField interfaceName_tf;
    Label package_lb;
    TextField package_tf;
    Label parent_lb;
    ChoiceBox parent_ccb;
    Label variables;
    Button variablesAdd_btn;
    Button variablesRemove_btn;
    ScrollPane variables_scrollPane;
    Label methods_lb;
    Button methodsAdd_btn;
    Button methodsRemove_btn;
    ScrollPane methods_scrollPane;
    Label name_lb;
    Label type_lb;
    Label static_lb;
    Label access_lb;
    Label return_lb;
    Label abstract_lb;
    
    Button argAdd_btn;
    Button argRemove_btn;
    DiagramGenerator diagram;
    
    public AddInterfaceUI() {
        
    }
    
    public void setUpLayout() {
        
    }
    
    public void setupHandler() {
        
    }
    
}
