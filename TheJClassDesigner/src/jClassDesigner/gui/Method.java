/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jClassDesigner.gui;

import jClassDesigner.Constants;
import jClassDesigner.controller.ComponentController;
import java.util.ArrayList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import saf.AppTemplate;

/**
 *
 * @author majiasheng
 */
public class Method extends HBox{
    private TextField name;
    private TextField returnType;
    private CheckBox staticStatus;
    private CheckBox abstractStatus;
    private ChoiceBox access;
    private ArrayList<TextField> args;
    private TextField arg1;
    
//    AppTemplate app;
//    ComponentController componentController;
    
    public Method(AppTemplate app) {
        super();
        
//        this.app = app;
        
        name = new TextField();
        returnType = new TextField();
        staticStatus = new CheckBox();
        abstractStatus = new CheckBox();
        access = new ChoiceBox();
        args = new ArrayList<>();
        arg1 = new TextField();
        args.add(arg1);
        this.getChildren().addAll(name, returnType, staticStatus, abstractStatus, access, arg1);
        initStyle();
        
    }
    
    public void setupHandler() {
//        componentController = new ComponentController(app);
        name.setOnAction(e -> {
//            componentController.handleEditMethodName();
            handleEditMethodName();
        });
        returnType.setOnAction(e -> {
//            componentController.handleEditReturnType();
            handleEditReturnType();
        });
        staticStatus.setOnAction(e -> {
//            componentController.handleEditMethodStatic();
            handleEditMethodStatic();
        });
        abstractStatus.setOnAction(e ->{
//            componentController.handleEditAbstractStatus();
            handleEditAbstractStatus();
        });
        access.setOnAction(e -> {
//            componentController.handleEditMethodAccess();
            handleEditMethodAccess();
        });
        
        
    }
    
    public void addArgument() {
//        TextField arg = new TextField();
//        this.getChildren().add(arg);
    }
    
    public void removeArgument() {
        
    }
    
    public void initStyle() {
        this.setSpacing(Constants.SPACING);
        this.setPrefWidth(Constants.VAR_METHOD_WIDTH);
        this.getStyleClass().add(Constants.HBOXES);
        
        name.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        returnType.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        staticStatus.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        abstractStatus.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        access.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        arg1.setPrefWidth(Constants.METHOD_BOX_WIDTH);
    }
    
    public String getName() {return name.getText();}
    public String getReturnType() {return returnType.getText();}
    //public String getStaticStatus() {return staticStatus.;}

    private void handleEditMethodName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditReturnType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditMethodStatic() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditAbstractStatus() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditMethodAccess() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
