package jClassDesigner.gui;

import jClassDesigner.Constants;
import static jClassDesigner.PropertyType.*;
import jClassDesigner.controller.ComponentController;
import jClassDesigner.controller.ToolController;
import jClassDesigner.data.DataManager;
import java.io.IOException;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import saf.controller.AppFileController;
import saf.ui.AppGUI;

/**
 *
 * @author Jia Sheng Ma (109696764)
 */
public class Workspace extends AppWorkspaceComponent {
            
    AppTemplate app;
    AppGUI gui;
    DataManager dataManager;
    ToolController toolController;
    ComponentController componentController;
    Constants constants;
    
    ArrayList<Button> toolbarButtons;
    HBox editToolbarPane;
    Button select_btn;
    Button resize_btn;
    Button addClass_btn;
    Button addInterface_btn;
    Button remove_btn;
    Button undo_btn;
    Button redo_btn;
    
    HBox viewToolbarPane;
    Button zoomIn_btn;
    Button zoomOut_btn;
    VBox gridSnap_vbox;
    CheckBox grid_cbx;
    CheckBox snap_cbx;
    
    // TEXTUAL INFO FOR MOUSE
    VBox text_vbox;
    Label coordinate;
    
    Pane componentToolbarPane;
    AddClassUI addClassUI;
    AddInterfaceUI addInterfaceUI;
    
    HBox containerPane;
    Pane workPane;
    ScrollPane workPane_scrollpane;
    
    public Workspace(AppTemplate app)  throws IOException{
        this.app = app;
        gui = app.getGUI();
        dataManager = (DataManager)app.getDataComponent();

        setUpLayout();
        setUpHandler();
        
    }
    
    /**
     * Sets up the layout of the user interface.
     */
    public void setUpLayout() {

        // SETUP EDIT TOOLBAR 
        editToolbarPane = new HBox();
        toolbarButtons = new ArrayList<>();
        select_btn = gui.initChildButton(editToolbarPane,       SELECT_ICON.toString(),         SELECT_TOOLTIP.toString(),      false);
        resize_btn = gui.initChildButton(editToolbarPane,       RESIZE_ICON.toString(),         RESIZE_TOOLTIP.toString(),      false);
        addClass_btn = gui.initChildButton(editToolbarPane,     ADDCLASS_ICON.toString(),       ADDCLASS_TOOLTIP.toString(),    false);
        addInterface_btn = gui.initChildButton(editToolbarPane, ADDINTERFACE_ICON.toString(),   ADDINTERFACE_TOOLTIP.toString(),  false);
        remove_btn = gui.initChildButton(editToolbarPane,       REMOVE_ICON.toString(),         REMOVE_TOOLTIP.toString(),      false);
        undo_btn = gui.initChildButton(editToolbarPane,         UNDO_ICON.toString(),           UNDO_TOOLTIP.toString(),        false);
        redo_btn = gui.initChildButton(editToolbarPane,         REDO_ICON.toString(),           REDO_TOOLTIP.toString(),        false);
            // ADD BUTTONS TO TOOLBAR
        gui.getTopToolbarPane().getChildren().add(editToolbarPane);
            // ADD BUTTONS TO LIST
        toolbarButtons.add(select_btn);
        toolbarButtons.add(resize_btn);
        toolbarButtons.add(addClass_btn);
        toolbarButtons.add(addInterface_btn);
        toolbarButtons.add(remove_btn);
        toolbarButtons.add(undo_btn);
        toolbarButtons.add(redo_btn);
        
        // SETUP VIEW TOOLBAR
        viewToolbarPane = new HBox();
        zoomIn_btn = gui.initChildButton(viewToolbarPane, ZOOMIN_ICON.toString(), ZOOMIN_TOOLTIP.toString(), false);
        zoomOut_btn = gui.initChildButton(viewToolbarPane, ZOOMOUT_ICON.toString(), ZOOMOUT_TOOLTIP.toString(), false);
        gridSnap_vbox = new VBox();
        grid_cbx = new CheckBox("Grid");        
        gridSnap_vbox.getChildren().add(grid_cbx);
        snap_cbx = new CheckBox("Snap");
        gridSnap_vbox.getChildren().add(snap_cbx);
        gridSnap_vbox.setAlignment(Pos.CENTER);
        viewToolbarPane.getChildren().add(gridSnap_vbox);
            // ADD BUTTONS TO TOOLBAR
        gui.getTopToolbarPane().getChildren().add(viewToolbarPane);
            // ADD BUTTONS TO LIST
        toolbarButtons.add(zoomIn_btn);
        toolbarButtons.add(zoomOut_btn);
        
        // TEXT FOR COORDINATES
        coordinate = new Label("(-, -)");
        text_vbox = new VBox(new Label("(mouseX, mouseY)"), coordinate);
        gui.getTopToolbarPane().getChildren().add(text_vbox);
        
        // COMPONENTS TOOLBAR
        componentToolbarPane = new Pane();
        addClassUI = new AddClassUI(app);
        addInterfaceUI = new AddInterfaceUI();
        
        // WORKSPACE
        workspace = new HBox();
        workPane = new Pane();
        workPane.setPrefSize(Constants.WORKPANE_WIDTH, Constants.WORKPANE_HEIGHT);
        workPane_scrollpane = new ScrollPane();
        workPane_scrollpane.setContent(workPane);
        
        workspace.getChildren().add(workPane_scrollpane);
            // ADD COMPONENT TOOLBAR CONTAINER
        workspace.getChildren().add(componentToolbarPane);
        
        // DISABLE TOOLS ON START UP
        disableTools();
        
        
    }
    
    /**
     * Sets up all the handlers for the controls/tools.
     */
    public void setUpHandler() {
        toolController = new ToolController(app);
        componentController = new ComponentController(app);
        
        workPane.addEventFilter(MouseEvent.ANY, e ->{
            String co = "(" + e.getX() + ", " + e.getY() + ")";
            coordinate.setText(co);
        });
        
        workPane.setOnMousePressed(e -> {
            //FIXME
            componentController.handleWorkPanePressed(e.getX(), e.getY(), e.isControlDown());
            
        });
        
        select_btn.setOnAction(e -> {
            toolController.handleSelect();
        });
        resize_btn.setOnAction(e -> {
            toolController.handleResize();
        });
        addClass_btn.setOnAction(e -> {
            componentController.handleAddClass();
        });
        
        addInterface_btn.setOnAction(e -> {
            componentController.handleAddInterface();
        });
        
        remove_btn.setOnAction(e -> {
            toolController.handleRemove();
        });
        
        undo_btn.setOnAction(e -> {
            toolController.handleUndo();
        });
        
        redo_btn.setOnAction(e -> {
            toolController.handleRedo();
        });
        
        zoomIn_btn.setOnAction(e -> {
            toolController.handleZoomIn();
        });
        
        zoomOut_btn.setOnAction(e -> {
            toolController.handleZoomOut();
        });
        
        grid_cbx.setOnAction(e -> {
            toolController.handleGridCheck();
        });
        
        snap_cbx.setOnAction(e -> {
            toolController.handleSnapCheck();
        });
    }
    
    /**
     * Disable all the tools.
     */
    public void disableTools() {
        editToolbarPane.setDisable(true);
        viewToolbarPane.setDisable(true);
    }
    
    /**
     * Enables all the tools.
     */
    public void enableTools() {
        editToolbarPane.setDisable(false);
        viewToolbarPane.setDisable(false);
    }
    
    public Pane getWorkPane() { return workPane; }
    public Pane getComponentToolbarPane() {return componentToolbarPane;}
    
    public AppTemplate getApp() {return app;}
            
    @Override
    public void reloadWorkspace() {
        
        
    }

    @Override
    public void initStyle() {
        
        workPane.getStyleClass().add(Constants.WORKSPACE_PANE);
        componentToolbarPane.getStyleClass().add(Constants.WORKSPACE_PANE);
        editToolbarPane.getStyleClass().add(Constants.EDIT_TOOLBAR_PANE);
        viewToolbarPane.getStyleClass().add(Constants.VIEW_TOOLBAR_PANE);
        gridSnap_vbox.getStyleClass().add(Constants.GRID_SNAP_VBOX);
        grid_cbx.getStyleClass().add(Constants.CHECKBOX);
        snap_cbx.getStyleClass().add(Constants.CHECKBOX);
        text_vbox.getStyleClass().add(Constants.TEXT_VBOX);
        
    }
    
}
