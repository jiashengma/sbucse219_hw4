package jClassDesigner.gui;

import jClassDesigner.Constants;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Jia Sheng Ma
 * @version 1.1
 */
public class DiagramGenerator extends VBox{
    
    private double initX;
    private double initY;
    private double init_translateX;
    private double init_translateY;
    
    Rectangle attributeBox;
    Rectangle methodBox;
    Label className;
    ArrayList<Label> attributes;
    ArrayList<Label> methods;
    
    public DiagramGenerator() {
        //super();
        
        attributes = new ArrayList<>();
        methods = new ArrayList<>();
        className = new Label(Constants.DEFAULT_CLASS_NAME);
        this.getChildren().add(className);
        
        initStyle();
    }
    
    /**
     * For creating diagrams in loading pre-existing info.
     * @param name class name
     * @param layoutX layout x of diagram
     * @param layoutY layout y of diagram
     */
    public DiagramGenerator(String name, double layoutX, double layoutY) {
        
        initStyle();
    }
    
    /**
     * Changes the class name in the diagram.
     * @param name class name of the diagram.
     */
    public void editClassName(String name) {
//        if(!name.trim().equals("")) {
            className.setText(name);
//        }
//        else {
//            Dialog alert = new Alert(Alert.AlertType.WARNING, "Class Name Cannot be Empty", ButtonType.OK);
//            alert.show();
//        }
    }
    
    public void editAttribute(Label lb, String s) {
        lb.setText(s);
    }
    
    public void editMethod(Label lb, String s) {
        lb.setText(s);
    }
    
    /**
     * @return the Class name of this UML class diagram.
     */
    public String getDiagramClassName() {
        return className.getText().trim();
    }
    
    /**
     * Used for loading in the diagrams only. 
     * Sets up the location of the diagram to where is was last saved.
     * @param translateX amount of x translation of the diagram.
     * @param translateY amount of y translation location of the diagram.
     */
    public void setupLocation(double translateX, double translateY) {
        this.setTranslateX(translateX);
        this.setTranslateY(translateY);
    }
    
    /**
     * Sets up the style of the diagrams.
     */
    public void initStyle() {
        this.setMinSize(Constants.DEFAULT_WIDTH, Constants.DEFAULT_HEIGHT);
        this.getStyleClass().add(Constants.DIAGRAM);
        //className.setAlignment(Pos.CENTER);
    }
    
}
