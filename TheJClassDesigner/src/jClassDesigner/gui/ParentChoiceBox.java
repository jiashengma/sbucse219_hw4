package jClassDesigner.gui;

import jClassDesigner.data.DataManager;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import saf.AppTemplate;

/**
 * This class contains all the available classes the designer created 
 * in a choice box so that the designer can choose appropriate classes
 * for the use of inheritance.
 * 
 * @author Jia Sheng Ma
 */
public class ParentChoiceBox extends ChoiceBox{
    AppTemplate app;
    DataManager dataManager;
    public static ObservableList<String> parentClasses;
    List parentClassNames;
    
    public ParentChoiceBox(AppTemplate app) {
        this.app = app;
        dataManager = (DataManager)app.getDataComponent();
        parentClassNames = dataManager.getClassNames();
        parentClasses = FXCollections.observableArrayList(parentClassNames);
        this.setItems(parentClasses);
    }
    
    /**
     * Add available classes to the parent_cbb for each class to choose parent class.
     * @return parent choice box that contains available classes for this class to extend.
     */
    public ObservableList<String> getParentClasses() {return parentClasses;}
    public void addParent(String parentName) {
        parentClasses.add(parentName);
    }
}
