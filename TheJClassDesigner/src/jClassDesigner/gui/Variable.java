package jClassDesigner.gui;

import jClassDesigner.Constants;
import jClassDesigner.controller.ComponentController;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import saf.AppTemplate;

/**
 *
 * @author majiasheng
 */
public class Variable extends HBox{
    
    private TextField name;
    private TextField type;
    private CheckBox staticStatus;
    private ChoiceBox access;
    
//    AppTemplate app;
//    ComponentController componentController;
    
    public Variable(AppTemplate app) {
        super();
//        this.app = app;
        
        name = new TextField();
        type = new TextField();
        staticStatus = new CheckBox();
        access = new ChoiceBox();
        this.getChildren().addAll(name, type, staticStatus, access);
        initStyle();
    }
    
    public void setupHandler() {
//        componentController = new ComponentController(app);
        name.setOnAction(e -> {
//            componentController.handleEditName();
            handleEditName();
        });
        type.setOnAction(e -> {
//            componentController.handleEditType();
            handleEditType();
        });
        staticStatus.setOnAction(e -> {
//            componentController.handleEditStatic();
            handleEditStatic();
        });
        access.setOnAction(e -> {
//            componentController.handleEditAccess();
            handleEditAccess();
        });
    }
    
    public void initStyle() {
        
        this.setSpacing(Constants.SPACING);
        this.setPrefWidth(Constants.VAR_METHOD_WIDTH);
        this.getStyleClass().add(Constants.HBOXES);
        
        name.setPrefWidth(Constants.VARIABLE_BOX_WIDTH);
        type.setPrefWidth(Constants.VARIABLE_BOX_WIDTH);
        staticStatus.setPrefWidth(Constants.VARIABLE_BOX_WIDTH);
        access.setPrefWidth(Constants.VARIABLE_BOX_WIDTH);
    }
    
    public String getName() {return name.getText();}
    public String getType() {return type.getText();}
    //public String getStaticStatus() {return staticStatus.;}

    private void handleEditName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditStatic() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditAccess() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
