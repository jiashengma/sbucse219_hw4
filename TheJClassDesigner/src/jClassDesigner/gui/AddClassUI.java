package jClassDesigner.gui;

import jClassDesigner.Constants;
import jClassDesigner.controller.ComponentController;
import jClassDesigner.data.DataManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import saf.AppTemplate;

/**
 *
 * @author Jia Sheng Ma
 * @version 1.0
 */
public class AddClassUI extends VBox{
    
    
    AppTemplate app;
    Workspace workspace;
    
    HBox class_hbox;
    private final Label className_lb = new Label("Class Name:");
    TextField className_tf;
    
    HBox package_hbox;
    private final Label package_lb = new Label("Package:");
    TextField package_tf;
    
    HBox parent_hbox;
    private final Label parent_lb = new Label("Parent:");
    ParentChoiceBox parent_ccb;
    
    VBox variableContainer;
    HBox variableLabels_hbox;
    HBox variableHeader_hbox;
    private final Label variables_lb = new Label("Variables:");
    Button variablesAdd_btn;
    Button variablesRemove_btn;    
    ScrollPane variables_scrollPane;
    
    VBox methodContainer;
    HBox methodLabels_hbox;
    HBox methodHeader_hbox;
    private final Label methods_lb = new Label("Methods:");
    Button methodsAdd_btn;
    Button methodsRemove_btn;
    ScrollPane methods_scrollPane;
    private final Label varName_lb = new Label("Name");
    private final Label methodName_lb = new Label("Name");
    private final Label type_lb = new Label("Type");
    private final Label varStatic_lb = new Label("Static");
    private final Label methodStatic_lb = new Label("Static");
    private final Label varAccess_lb = new Label("Access");
    private final Label methodAccess_lb = new Label("Access");
    private final Label return_lb = new Label("Return");
    private final Label abstract_lb = new Label("Abstract");
    private final Label arg1_lb = new Label("Arg1");
    
    Button argAdd_btn;
    Button argRemove_btn;
    DiagramGenerator diagram;
    Method method;
    Variable variable;
    
    String className;
    String packageName;
    
    public AddClassUI(AppTemplate app) {
        super();
        this.app = app;
        diagram = new DiagramGenerator();
        
        // SET UP DRAGGABLE
        ComponentController componentController = new ComponentController(app);
        componentController.setupDraggable(diagram);
        
        setUpLayout();
        setupHandler();
        initStyle();

    }
    
    public void setUpLayout() {
        // CLASS NAME
        class_hbox = new HBox();
        className_tf = new TextField();
        className_tf.setPromptText("Class Name");
        class_hbox.getChildren().addAll(className_lb, className_tf);
        this.getChildren().add(class_hbox);        
        className = className_tf.getText();
        
        // PACKAGE
        package_hbox = new HBox();
        package_tf = new TextField();
        package_tf.setPromptText("Package Name");
        package_hbox.getChildren().addAll(package_lb, package_tf);
        this.getChildren().add(package_hbox);
        packageName = package_tf.getText();
        
        // PARENT
        parent_hbox = new HBox();
        parent_ccb = new ParentChoiceBox(app);
        parent_hbox.getChildren().addAll(parent_lb, parent_ccb);
        this.getChildren().add(parent_hbox);
        
        //VARIABLE
        variableContainer = new VBox();
            
            // VARIABLE HEADER
        variableHeader_hbox = new HBox();
        variablesAdd_btn = new Button("+");
        variablesRemove_btn = new Button("-");
        variableHeader_hbox.getChildren().addAll(variables_lb, variablesAdd_btn, variablesRemove_btn);
        variableContainer.getChildren().add(variableHeader_hbox);

            // VARIABLE LABELS
        variableLabels_hbox = new HBox();
        variableLabels_hbox.getChildren().addAll(varName_lb, type_lb, varStatic_lb, varAccess_lb);
        variableContainer.getChildren().add(variableLabels_hbox);
        
            // VARIABLE INPUTS
        variable = new Variable(app);
        variableContainer.getChildren().add(variable);
        
        variables_scrollPane = new ScrollPane();
        variables_scrollPane.setContent(variableContainer);
        this.getChildren().add(variables_scrollPane);
        
        // METHOD
        methodContainer = new VBox();
            
            // METHOD HEADER
        methodHeader_hbox = new HBox();
        methodsAdd_btn = new Button("+");
        methodsRemove_btn = new Button("-"); 
        methodHeader_hbox.getChildren().addAll(methods_lb, methodsAdd_btn, methodsRemove_btn);
        methodContainer.getChildren().add(methodHeader_hbox);
        
            // METHOD LABELS
        methodLabels_hbox = new HBox();
        methodLabels_hbox.getChildren().addAll(methodName_lb, return_lb, methodStatic_lb, abstract_lb, methodAccess_lb, arg1_lb);
        methodContainer.getChildren().add(methodLabels_hbox);
        
            // METHOD INPUTS
        method = new Method(app);
        methodContainer.getChildren().add(method);
        
        methods_scrollPane = new ScrollPane();
        methods_scrollPane.setContent(methodContainer);
        this.getChildren().add(methods_scrollPane);
        
    }
    
    public void setupHandler() {
        ComponentController componentController = new ComponentController(app);

        className_tf.addEventHandler(KeyEvent.ANY, e->{
            diagram.editClassName(className_tf.textProperty().getValue());
        });
        
        className_tf.setOnAction(e -> {
            String className = className_tf.textProperty().getValue().trim();
            diagram.editClassName(className_tf.getText());
            componentController.handleEditClassName(this, className_tf, className);
        });
        
        package_tf.setOnAction(e -> {
            String packageName = package_tf.getText().trim();
            componentController.handleEditPackageName(this, package_tf, packageName);
        });
        
    }
    
    public TextField getClassName_tf() {return className_tf;}
    public TextField getPackageName_tf() {return package_tf;}
    
    public void setClassName(String className) {
        this.className = className;
    }
    public String getClassName() {
//        return className_tf.getText();
        return className;
    }
    
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    public String getPackageName() {
        return package_tf.getText();
    }
    
    public DiagramGenerator getDiagram() {return diagram;}
    
    public Variable getVariable() {return variable;}
    public Method getMethod() {return method;}
    
    public void initStyle() {
        
        this.setSpacing(Constants.CLASSUI_SPACING);
        this.setPrefSize(Constants.COMPONENT_TOOLBAR_WIDTH, Constants.COMPONENT_TOOLBAR_HEIGHT);
        this.getStyleClass().add(Constants.CLASSUI);
        
        variableHeader_hbox.getStyleClass().add(Constants.HBOXES);
        variableLabels_hbox.getStyleClass().add(Constants.HBOXES);
        methodHeader_hbox.getStyleClass().add(Constants.HBOXES);
        methodLabels_hbox.getStyleClass().add(Constants.HBOXES);
        
        className_lb.setPrefWidth(Constants.HBOXES_WIDTH);
        className_tf.setPrefWidth(Constants.HBOXES_WIDTH);
        package_lb.setPrefWidth(Constants.HBOXES_WIDTH);
        package_tf.setPrefWidth(Constants.HBOXES_WIDTH);
        parent_lb.setPrefWidth(Constants.HBOXES_WIDTH);
        parent_ccb.setPrefWidth(Constants.HBOXES_WIDTH);
        
        variableLabels_hbox.setSpacing(Constants.SPACING);
        variableHeader_hbox.setSpacing(Constants.SPACING);
        variableContainer.getStyleClass().add(Constants.VBOXES);
        variables_scrollPane.setPrefWidth(Constants.VAR_METHOD_SCROLLPANE_WIDTH);
        variables_scrollPane.setPrefHeight(Constants.VARIABLE_BOX_HEIGHT*3);
        
        methodLabels_hbox.setSpacing(Constants.SPACING);
        methodHeader_hbox.setSpacing(Constants.SPACING);
        methodContainer.getStyleClass().add(Constants.VBOXES);
        methods_scrollPane.setPrefWidth(Constants.VAR_METHOD_SCROLLPANE_WIDTH);
        methods_scrollPane.setPrefHeight(Constants.METHOD_BOX_HEIGHT*3);
        
        varAccess_lb.setPrefWidth(Constants.VARIABLE_BOX_WIDTH);
        varName_lb.setPrefWidth(Constants.VARIABLE_BOX_WIDTH);
        varStatic_lb.setPrefWidth(Constants.VARIABLE_BOX_WIDTH);
        methodName_lb.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        type_lb.setPrefWidth(Constants.VARIABLE_BOX_WIDTH);
        methodStatic_lb.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        methodAccess_lb.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        return_lb.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        abstract_lb.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        arg1_lb.setPrefWidth(Constants.METHOD_BOX_WIDTH);
    }
    
}
