package jClassDesigner.data;

import jClassDesigner.gui.Workspace;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.paint.Color;
import javafx.scene.layout.VBox;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Jia Sheng Ma
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    
    AppState state;

    AppTemplate app;
    
    private HashMap<VBox, VBox> componentUIList;    // K = DIAGRAM, V = UI
    private ArrayList<VBox> diagrams;
    
    // FOR CHECKING DUPLICATES AND EXPORTING
    private ArrayList<String> classNames;
    private ArrayList<String> packageNames;
    
    private String code;
    
    private Node prev_selectedNode;
    private Node selectedNode;
    
    private Effect highlightedEffect;

    public DataManager(AppTemplate initApp) throws Exception {
	
	app = initApp;
        
        componentUIList = new HashMap<>();
        diagrams = new ArrayList<>();
        
        classNames = new ArrayList<>();
        packageNames = new ArrayList<>();
        
        prev_selectedNode = null;
        selectedNode = null;
        
	// EFFECT FOR HIGHLIGHTING SELECTED NODE
	DropShadow dropShadowEffect = new DropShadow();
	dropShadowEffect.setOffsetX(0.0f);
	dropShadowEffect.setOffsetY(0.0f);
	dropShadowEffect.setSpread(1.0);
	dropShadowEffect.setColor(Color.YELLOW);
	dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
	dropShadowEffect.setRadius(10);
	highlightedEffect = dropShadowEffect;
    }
    
    /**
     * Sets the node that's being selected, and highlights it.
     * @param node selected node
     */
    public void setSelectedNode(Node node) {
        prev_selectedNode = selectedNode;
        
        if(prev_selectedNode != null) {
            // CLEAR HIGHLIGHT OF THE PREVIOUS SELECTED NODE
            prev_selectedNode.setEffect(null);
        }
        
        selectedNode = node;
        
        // HIGHLIGHT IF THE NODE SELECTED IS NOT NULL
        if(selectedNode!=null) {
            selectedNode.setEffect(highlightedEffect);
        }
        
    }
    
    public Node getSelectedNode() {
        return selectedNode;
    }
    
    public void addComponentUI(VBox diagram, VBox ui) {
        componentUIList.put(diagram, ui);
    }
    
    //public ArrayList<VBox> getComponentUI() {return componentUIList;}
    public HashMap<VBox, VBox> getComponentUIs() {
        return componentUIList;
    }
    public VBox getComponentUI(VBox diagram) {
        return componentUIList.get(diagram);
    }
    
    public void addClassName(String name) {
        classNames.add(name);
    } 
    public ArrayList<String> getClassNames() {
        return classNames;
    }
    
    public void addPackage(String packageName) {
        packageNames.add(packageName);
    }
    public ArrayList<String> getPackageNames() {
        return packageNames;
    }
    
    public void addDiagram(VBox diagram) {
        diagrams.add(diagram);
    }
    public ArrayList<VBox> getDiagrams() {
        return diagrams;
    }
    
//    public void addDiagram(VBox r) {diagrams.put(r, r);}
//    public HashMap<VBox,VBox> getDiagrams() {return diagrams;}
//    public VBox getDiagram(VBox vbox) {
//        return diagrams.get(vbox);
//    }
    
    public void writeCode() {};
    public String getCode() {
        return code;
    }
    
    public void setAppState(AppState state) {
        this.state = state;
    }
    public AppState getAppState() {
        return state;
    }
    
    public AppTemplate getApp() {return app;}
    
    /**
     * Resets the application to new.
     */
    @Override
    public void reset() {
        state = AppState.NEW_STATE;
        
        // CLEAR THE WORKSPACE
        ((Workspace)app.getWorkspaceComponent()).getWorkPane().getChildren().clear();
        ((Workspace)app.getWorkspaceComponent()).getComponentToolbarPane().getChildren().clear();
    }
}
