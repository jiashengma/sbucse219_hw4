
package jClassDesigner.test_bed;

import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.event.Event;
import javafx.event.EventHandler;
import java.util.Date;
import javafx.application.Platform;
import javafx.concurrent.Task;
/**
 *
 * @author Jia Sheng Ma
 */
public class TestSave {
    
    public static final String START_TEXT = "Start";
    public static final String PAUSE_TEXT = "Pause";
    private Stage window;
    private BorderPane appPane;
    private FlowPane topPane;
    private Button startButton;
    private Button pauseButton;
    private ScrollPane scrollPane;
    private TextArea textArea;
    private Thread dateThread;
    private Task dateTask;
    private Thread counterThread;
    private Task counterTask;
    private boolean work;
//
//    public void start(Stage primaryStage) throws Exception {
//	initLayout();
//	initHandlers();
//	initWindow(primaryStage);
//	initThreads();
//	work = false;
//    }
//    
//    public void startWork() {
//	work = true;
//    }
//    
//    public void pauseWork() {
//	work = false;
//    }
//    
//    public boolean doWork() {
//	return work;
//    }
//
//    public void appendText(String textToAppend) {
//	textArea.appendText(textToAppend);
//    }
//
//    public void sleep(int timeToSleep) {
//	try {
//	    Thread.sleep(timeToSleep);
//	} catch (InterruptedException ie) {
//	    ie.printStackTrace();
//	}
//    }
//
//    private void initLayout() {
//	topPane = new FlowPane(Orientation.HORIZONTAL);
//	startButton = new Button(START_TEXT);
//	pauseButton = new Button(PAUSE_TEXT);
//	topPane.getChildren().add(startButton);
//	topPane.getChildren().add(pauseButton);
//
//	textArea = new TextArea();
//	scrollPane = new ScrollPane();
//	scrollPane.setContent(textArea);
//
//	appPane = new BorderPane();
//	appPane.setTop(topPane);
//	appPane.setCenter(scrollPane);
//    }
//    
//    private void initHandlers() {
//	startButton.setOnAction(new StartHandler(this));
//	pauseButton.setOnAction(new PauseHandler(this));
//    }
//
//    private void initWindow(Stage initPrimaryStage) {
//	window = initPrimaryStage;
//	Scene scene = new Scene(appPane, 800, 600);
//	window.setScene(scene);
//	window.show();
//    }
//
//    private void initThreads() {
//	dateTask = new DateTask(this);
//	dateThread = new Thread(dateTask);
//	dateThread.start();
//
//	counterTask = new CounterTask(this);
//	counterThread = new Thread(counterTask);
//	counterThread.start();
//    }
//
//}
//class StartHandler implements EventHandler {
//    private ThreadExample app;
//    
//    public StartHandler(ThreadExample initApp) {
//	app = initApp;
//    }
//    
//    @Override
//    public void handle(Event event) {
//	app.startWork();
//    }
//    
//}
//
//class PauseHandler implements EventHandler {
//    private ThreadExample app;
//    
//    public PauseHandler(ThreadExample initApp) {
//	app = initApp;
//    }
//    
//    @Override
//    public void handle(Event event) {
//	app.pauseWork();
//    }
//    
//}
//
//class DateTask extends Task<Void> {
//
//    private ThreadExample app;
//    private Date now;
//
//    public DateTask(ThreadExample initApp) {
//	app = initApp;
//    }
//
//    @Override
//    protected Void call() throws Exception {
//	while (true) {
//	    if (app.doWork()) {
//		now = new Date();
//		Platform.runLater(new Runnable() {
//		    @Override
//		    public void run() {
//			String time = now.toString() + "\n";
//			app.appendText(time);
//		    }
//		});
//
//		// SLEEP EACH FRAME
//		app.sleep(10);
//	    } else {
//		app.sleep(10);
//	    }
//	}
//    }
//}
//
//class CounterTask extends Task<Void> {
//
//    private ThreadExample app;
//    private int counter;
//
//    public CounterTask(ThreadExample initApp) {
//	app = initApp;
//	counter = 0;
//    }
//
//    @Override
//    protected Void call() throws Exception {
//	while (true) {
//	    if (app.doWork()) {
//		Platform.runLater(new Runnable() {
//		    @Override
//		    public void run() {
//			counter++;
//			String text = "\tIteration: " + counter + "\n";
//			app.appendText(text);
//		    }
//		});
//
//		// SLEEP EACH FRAME
//		app.sleep(10);
//	    } else {
//		app.sleep(10);
//	    }
//	}
//    }
}
