package jClassDesigner.controller;

import jClassDesigner.data.AppState;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.shape.Shape;
import jClassDesigner.data.DataManager;
import jClassDesigner.gui.DiagramGenerator;
import jClassDesigner.gui.Workspace;
import javafx.scene.layout.VBox;
import saf.AppTemplate;
import saf.ui.AppGUI;

/**
 * This class responds to interactions with the rendering surface.
 * 
 * @author Jia Sheng Ma
 * @version 1.0
 */
public class ToolController {
    AppTemplate app;
    DataManager dataManager;
    AppState state;
    AppGUI gui;
    
    public ToolController(AppTemplate initApp) {
	app = initApp;
        gui = app.getGUI();
        dataManager = (DataManager)app.getDataComponent();
    }

    public void handleSelect() {
        dataManager.setAppState(AppState.SELECT_STATE);
    }

    public void handleResize() {
        dataManager.setAppState(AppState.RESIZE_STATE);
    }

    /**
     * Removes selected class diagram and its corresponding component ui.
     */
    public void handleRemove() {
        VBox selectedNode = (VBox)dataManager.getSelectedNode();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        if(dataManager.getSelectedNode() != null) {
            // REMOVE DIAGRAM FROM DATA MANAGER 
            dataManager.getDiagrams().remove(selectedNode);
            // AND FROM WORK PANE
            workspace.getWorkPane().getChildren().remove((DiagramGenerator)selectedNode);
            
            // REMOVE ITS CORRESPONDING COMPONENT UI FROM DATA MANAGER 
            dataManager.getComponentUIs().remove(selectedNode);
            // AND FROM COMPONENT TOOL BAR
            workspace.getComponentToolbarPane().getChildren().clear();
        }
        // MARK AS UNSAVED
        app.getGUI().updateToolbarControls(false);
        
    }

    public void handleUndo() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        /*DataManager */dataManager = (DataManager)app.getDataComponent();
        
        app.getGUI().updateToolbarControls(false);
    }

    public void handleRedo() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        /*DataManager */dataManager = (DataManager)app.getDataComponent();
        
        app.getGUI().updateToolbarControls(false);
    }

    public void handleZoomIn() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        //workspace.getWorkPane().setScaleX();
    }

    public void handleZoomOut() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        //workspace.getWorkPane().setScaleX();
    }

    public void handleGridCheck() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        app.getGUI().updateToolbarControls(false);
    }

    public void handleSnapCheck() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        app.getGUI().updateToolbarControls(false);
        
    }
    
}
