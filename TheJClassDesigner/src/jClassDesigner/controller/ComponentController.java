package jClassDesigner.controller;

import jClassDesigner.data.DataManager;
import jClassDesigner.data.AppState;
import jClassDesigner.gui.AddClassUI;
import jClassDesigner.gui.DiagramGenerator;
import jClassDesigner.gui.ParentChoiceBox;
import jClassDesigner.gui.Workspace;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import saf.AppTemplate;

/**
 * This class responds to interactions with other UI pose editing controls.
 * 
 * @author Jia Sheng Ma
 * @version 1.0
 */
public class ComponentController {

    private final String PACKAGE_EXIST_ERROR = "Package already exist, please choose another name.";
    private final String CLASS_EXIST_ERROR = "Class already exist, please choose another name.";
    
    private final Dialog package_exist_alert = new Alert(Alert.AlertType.WARNING, PACKAGE_EXIST_ERROR, ButtonType.OK);
    private final Dialog class_exist_alert = new Alert(Alert.AlertType.WARNING, CLASS_EXIST_ERROR, ButtonType.OK);
    
    AppTemplate app;
    AppState state;

    DataManager dataManager;
    
    double initX;
    double initY;
    double init_translateX;
    double init_translateY;
    
    public ComponentController(AppTemplate initApp) {
	app = initApp;
	dataManager = (DataManager)app.getDataComponent();
    }

    public void handleAddClass() {

        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        AddClassUI classUI = new AddClassUI(app);
        ParentChoiceBox parents = new ParentChoiceBox(app);
        DiagramGenerator diagram = classUI.getDiagram();
        //setupDraggable(diagram);  // DRAGGABLE SHOULD BE SET UP AFTER ITS CREATION
        
        // SET THE NEWLY CREATED DIAGRAM AS THE SELECTED NODE
        dataManager.setSelectedNode(diagram);
        
        // ADD A COPY TO DATAMANAGER
        dataManager.addDiagram(diagram);
        dataManager.addComponentUI(diagram, classUI);
        
        // ADD CLASS NAME TO DATA MANAGER
        //dataManager.addParentClassNameToList(classUI.getName());
        
        // ADD DIAGRAM TO WORK PANE FOR DISPLAY
        workspace.getWorkPane().getChildren().add(diagram);
// DEBUG:
//        System.out.println("layout x: " + diagram.getLayoutX() + "\nlayout y: " + diagram.getLayoutY());
        
        // CLEAR CURRENT UI
        workspace.getComponentToolbarPane().getChildren().clear();
        // ADD A NEW UI
        workspace.getComponentToolbarPane().getChildren().add(classUI);
        
        // ADD CLASS TO PARENT CHOICE BOX
        // FIXME:
        //parents.addParent(classUI.getName());
        
        workspace.getComponentToolbarPane().setDisable(false);
        
    }
    
    public void handleAddInterface() {
        
    }

    public void handleEditName() {
        System.out.println("Nothing yet");
    }

    public void handleEditType() {
        System.out.println("Nothing yet");
    }

    public void handleEditStatic() {
        System.out.println("Nothing yet");
    }

    public void handleEditAccess() {
        System.out.println("Nothing yet");
    }

    public void handleEditMethodName() {
        System.out.println("Nothing yet");
    }

    public void handleEditReturnType() {
        System.out.println("Nothing yet");
    }

    public void handleEditMethodStatic() {
        System.out.println("Nothing yet");
    }

    public void handleEditMethodAccess() {
        System.out.println("Nothing yet");
    }

    public void handleEditAbstractStatus() {
        System.out.println("Nothing yet");
    }
    
    /**
     * Sets up the node as draggable.
     * @param node node to be set as draggable.
     */
    public void setupDraggable(Node node) {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, (final MouseEvent mouseEvent) -> {
            if(dataManager.getAppState()==AppState.SELECT_STATE) {
                initX = mouseEvent.getSceneX();
                initY = mouseEvent.getSceneY();
                init_translateX = node.getTranslateX();
                init_translateY = node.getTranslateY();
                dataManager.setSelectedNode(node);
                
                // CLEAR COMPONENT TOOLBAR PANE
                workspace.getComponentToolbarPane().getChildren().clear();
        
                // ADD SELECTED DIAGRAM'S COMPOENENT TOOLBAR PANE
                VBox ui = dataManager.getComponentUI((VBox)node);
                workspace.getComponentToolbarPane().getChildren().add(ui);
                workspace.getComponentToolbarPane().setDisable(false);
            }
        });

        node.addEventFilter(MouseEvent.MOUSE_DRAGGED, (final MouseEvent mouseEvent) -> {
            if(dataManager.getAppState()==AppState.SELECT_STATE) {
                node.setTranslateX(init_translateX + mouseEvent.getSceneX() - initX);
                node.setTranslateY(init_translateY + mouseEvent.getSceneY() - initY);
// DEBUG
//                System.out.println("layout x: " + node.getLayoutX()+ "\nlayout y: " + node.getLayoutY());
                
                // SET TRANSLATE X AND Y OF NODE
                
                // UPDATE TOOLBAR CONTROL
                app.getGUI().updateToolbarControls(false);
            }
            
        });
    }

    public void updataParentChoiceBox() {
        
    }

    /**
     * 
     * @param ui AddClassUI/InterfaceUI corresponding to the diagram selected.
     * @param tf package name input text field
     * @param packageName package name
     */
    public void handleEditPackageName(VBox ui, TextField tf, String packageName) {
        //TODO: Should not allow entry of package name if its package/class combo already exists in design
        String name = packageName.trim();
//        if(dataManager.getPackageNames().contains(name)) {
//            // FIXME: DOUBLE ENTER WOULD CAUSE ERROR
//            package_exist_alert.show();
//            
//            // CLEAR TEXTFIELD
//            tf.clear();
//            
//        } else {
            // REMOVE OLD PACKAGE NAME FROM DATA MANAGER
            if(ui instanceof AddClassUI) {
                dataManager.getPackageNames().remove(((AddClassUI)ui).getPackageName());
            }
            
            if(name != null) {
                // ADD PACKAGE NAME TO DATA MANAGER                
                dataManager.addPackage(name);
                ((AddClassUI)ui).setPackageName(name);
            }
//            for(String s : dataManager.getPackageNames()) {
//                System.out.println(s);
//            }
//        }
    }
    
    public void handleEditClassName(AddClassUI ui, TextField tf, String className) {
        String name = className.trim();
       if(dataManager.getClassNames().contains(name)) {
            class_exist_alert.show();
            
            // CLEAR TEXTFIELD
            tf.clear();
            
        } else {
            // REMOVE OLD CLASS NAME FROM DATA MANAGER
            dataManager.getClassNames().remove(ui.getClassName());
            
            if(!name.equals("")) {
                // ADD CLASS NAME TO DATA MANAGER
                dataManager.addClassName(name);
                ui.setClassName(name);
            }
//            else {
//                Dialog alert = new Alert(Alert.AlertType.WARNING, "Class Name Cannot be Empty", ButtonType.OK);
//                alert.show();
//            }
        }
    }
    
    /**
     * Un-highlights.
     * @param x mouse x
     * @param y mouse y
     */
    public void handleWorkPanePressed(double x, double y, boolean isControlDown) {
        // IF THE SELECTED NODE IS NULL, DESELECT THE CURRENT SELECTED SHAPE
        if(!isNodeClicked(x, y) && isControlDown) {
            unHighlight();
            dataManager.setSelectedNode(null);
            
            // TODO: DISABLE COMPONENT TOOL BAR
            Workspace workspace = (Workspace)app.getWorkspaceComponent();
            workspace.getComponentToolbarPane().setDisable(true);
        }
        
    }
    
    public void unHighlight() {
        if(dataManager.getSelectedNode()!=null) {
            dataManager.getSelectedNode().setEffect(null);
        }
    }
    
    public boolean isNodeClicked(double x, double y) {
        //ArrayList<VBox> diagrams = dataManager.getDiagrams();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        ObservableList<Node> nodes = workspace.getWorkPane().getChildren();
        boolean isClicked = false;
        for(int i = nodes.size()-1; i > 0; i--) {
            if(nodes.get(i).contains(x,y)) {
                isClicked = true;
                break;
            }
        }
	return isClicked;
    }

    
}
