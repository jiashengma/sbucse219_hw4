package jClassDesigner.file;

import jClassDesigner.controller.ComponentController;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import jClassDesigner.data.DataManager;
import jClassDesigner.gui.AddClassUI;
import jClassDesigner.gui.DiagramGenerator;
import jClassDesigner.gui.Workspace;
import javafx.scene.layout.VBox;
import saf.AppTemplate;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author Jia Sheng Ma
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    
    AppTemplate app;
    
    // FOR JSON LOADING
        // DIAGRAM PROPERTIES
    static final String DIAGRAM_CLASS_NAME = "diagram_class_name";
    static final String LAYOUT_X = "diagram_layoutX";
    static final String LAYOUT_Y = "diagram_layoutY";
    static final String DIAGRAM_WIDTH = "diagram_width";
    static final String DIAGRAM_HEIGHT = "diagram_height";
    static final String VARIABLES = "variables";
    static final String METHODS = "methods";
    
    static final String DIAGRAMS = "diagrams";
    static final String COMPONENT_UIS = "component_uis";
    static final String CLASS_NAME = "class_name";
    static final String PACKAGE_NAME = "package_name";
    static final String TRANSLATE_X= "translate_X";
    static final String TRANSLATE_Y= "translate_Y";
            
        // COMPONENT UI PROPERTIES
//    static final String 
 
    /**
     * Default contructor.
     * @param app this application.
     */
    public FileManager(AppTemplate app) {

        this.app = app;
    }
    
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	DataManager dataManager = (DataManager)data;
	
//	// GET DIAGRAM 
//        ArrayList<VBox> diagrams = dataManager.getDiagrams();
//        // CREATE JSON ARRAY BUILDER 
//        JsonArrayBuilder diagramArrayBuilder = Json.createArrayBuilder();
//        // FILL JSON ARRAY BUILDER WITH DIAGRAMS - TO MAKE A DIAGRAM JSON ARRAY
//        fillJABuilderWithDiagram(diagrams, diagramArrayBuilder);
//	JsonArray diagramArray = diagramArrayBuilder.build();
        
        
        // GET UI COMPONENTS 
        HashMap<VBox, VBox> componentUIs = dataManager.getComponentUIs();
        // CREATE JSON ARRAY BUILDER 
        JsonArrayBuilder componentUIsArrayBuilder = Json.createArrayBuilder();
        // FILL JSON ARRAY BUILDER WITH UI COMPONENTS - TO MAKE A UI COMPONENT JSON ARRAY
        fillJABuilderWithComponentUI(componentUIs, componentUIsArrayBuilder);
        JsonArray componentUIsArray = componentUIsArrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(COMPONENT_UIS, componentUIsArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    public JsonObject makeDiagramJsonObject(DiagramGenerator diagram) {
	JsonObject diagramJson = Json.createObjectBuilder()
		.add(DIAGRAM_CLASS_NAME, diagram.getDiagramClassName())
                .add(LAYOUT_X, diagram.getLayoutX()+"")                 // LAYOUT X AND Y ARE "0" , CHECK
                .add(LAYOUT_Y, diagram.getLayoutY()+"")
//              .add(VARIABLES, diagram.getVariables)
//		.add(METHODS, diagram.getMethods)
                .build();
	return diagramJson;
    }
    private void fillJABuilderWithDiagram(ArrayList<VBox> diagrams, JsonArrayBuilder arrayBuilder) {	
        for(VBox diagram : diagrams) {            
            JsonObject diagramObject = makeDiagramJsonObject((DiagramGenerator) diagram);
            arrayBuilder.add(diagramObject);
        }
    }
    
    public JsonObject makeComponentUIJsonObject(VBox ui) {
        // TODO: DO ADDCLASS UI FIRST, INTERFACE UI LATER
        
//        double diagram_layoutX = ((AddClassUI)ui).getDiagram().getLayoutX();
//        double diagram_layoutY = ((AddClassUI)ui).getDiagram().getLayoutY();
        
        double diagram_translateX = ((AddClassUI)ui).getDiagram().getTranslateX();
        double diagram_translateY = ((AddClassUI)ui).getDiagram().getTranslateY();
        
        JsonObject componentUIJson = Json.createObjectBuilder()
                .add(CLASS_NAME, ((AddClassUI)ui).getClassName())
                .add(PACKAGE_NAME, ((AddClassUI)ui).getPackageName())
                //.add(PARENT, )
                //.add(VARIABLES , )
                //.add(METHODS , )
                //.add(LAYOUT_X, diagram_layoutX+"")
                //.add(LAYOUT_Y, diagram_layoutY+"")
                .add(TRANSLATE_X, diagram_translateX+"")
                .add(TRANSLATE_Y, diagram_translateY+"")
                .build();
        return componentUIJson;
    }
    
    /**
     * 
     * @param componentUIs 
     * @param arrayBuilder 
     */
    public void fillJABuilderWithComponentUI(HashMap<VBox, VBox> componentUIs, JsonArrayBuilder arrayBuilder) {
        for(VBox ui : componentUIs.values()) {
            JsonObject componentUIObject = makeComponentUIJsonObject(ui);
            arrayBuilder.add(componentUIObject);
        }
    }
    
    
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	JsonArray componentUIsArray = json.getJsonArray(COMPONENT_UIS);
	
	// LOAD COMPONENT UIS(WITH DIAGRAMS)
        loadComponentUIs(componentUIsArray, dataManager);
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    /**
     * Loads component UI
     * @param jso
     * @return 
     */
    private VBox loadComponentUI(JsonObject jso) {
        String class_name = jso.getString(CLASS_NAME);
        String package_name = jso.getString(PACKAGE_NAME);
        double diagram_translateX = Double.parseDouble(jso.getString(TRANSLATE_X));
        double diagram_translateY = Double.parseDouble(jso.getString(TRANSLATE_Y));
        
        // CREATE COMPONENT UI WITH LOADED DATA
        AddClassUI componentUI = new AddClassUI(app);
            // SET VALUES
        componentUI.getClassName_tf().setText(class_name);
        componentUI.setClassName(class_name);
        componentUI.getPackageName_tf().setText(package_name);
        componentUI.setPackageName(package_name);
        
        DiagramGenerator diagram = componentUI.getDiagram();
            //ADD DIAGRAM TO WORK PANE
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getWorkPane().getChildren().add(diagram);
            // SETUP DIAGRAM'S LOCATION
        diagram.setupLocation(diagram_translateX, diagram_translateY);
        
        return componentUI;
    }
    
    private void loadComponentUIs(JsonArray componentUIsArray, DataManager dataManager) {
//        ComponentController componentController = new ComponentController(app);
        for(int i = 0; i < componentUIsArray.size(); i++) {
            JsonObject jso = componentUIsArray.getJsonObject(i);
            AddClassUI classUI = (AddClassUI)loadComponentUI(jso);
            DiagramGenerator diagram = classUI.getDiagram();
            
            // ADD LOADED DATA TO DATA MANAGER
            dataManager.addComponentUI(diagram, classUI);
            
        }
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        
    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {

    }
}
